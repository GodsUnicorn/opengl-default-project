#ifndef GLWINDOW_H
#define GLWINDOW_H

#include <iostream>
#include <glew.h>
#include <glfw3.h>

class GLWindow {
private:
	GLFWwindow* window;
	const char* name;
	GLuint width, height;
	GLint bufferWidth, bufferHeight;

	void Initialize();
	void Clear();
public:
	GLWindow(); 
	
	GLWindow(GLuint windowWidth, GLuint windowHeight, const char* windowName);
	
	void SwapBuffers();

	GLint GetBufferWidth();
	GLint GetBufferHeight();

	bool WindowShouldClose();


	~GLWindow();
};



#endif // !GLWINDOW_H