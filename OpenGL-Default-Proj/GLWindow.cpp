#include "GLWindow.h"

GLWindow::GLWindow() {
	window = 0;
	name = "Window";
	width = 800, height = 600;
	bufferWidth = 0, bufferHeight = 0;

	Initialize();
}

GLWindow::GLWindow(GLuint windowWidth, GLuint windowHeight, const char* windowName) {
	window = 0;
	name = windowName;
	width = windowWidth, height = windowHeight;
	bufferWidth = 0, bufferHeight = 0;

	Initialize();
} 

void GLWindow::Initialize() {
	if (!glfwInit()) {
		std::cout << "Failed to initialize glfw." << std::endl;
		system("pause");
		return;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	 window = glfwCreateWindow(width, height, name, 0, 0);

	if (!window) {
		std::cout << "GLFW failed to create widow." << std::endl;
		system("pause");
		glfwTerminate();
		return;
	}

	glfwGetFramebufferSize(window, &bufferWidth, &bufferHeight);
	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK) {
		std::cout << "Failed to initialize GLEW." << std::endl;
		system("pause");
		glfwTerminate();
		return;
	}

	glEnable(GL_DEPTH_TEST);

	glViewport(0, 0, bufferWidth, bufferHeight);
}

void GLWindow::Clear() {
	glfwTerminate();
}

void GLWindow::SwapBuffers() { glfwSwapBuffers(window); }

GLint GLWindow::GetBufferWidth() { return bufferWidth; }
GLint GLWindow::GetBufferHeight() { return bufferHeight; }

bool GLWindow::WindowShouldClose() { return glfwWindowShouldClose(window); }

GLWindow::~GLWindow() {
	Clear();
}
