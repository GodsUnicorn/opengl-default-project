#include <iostream>
#include <cmath>
#include <glew.h>
#include <glfw3.h>

#include <GLM/GLM/glm.hpp>
#include <GLM/GLM/gtc/type_ptr.hpp>
#include <GLM/GLM/mat4x4.hpp>

#include "GLWindow.h"
#include "Mesh.h"
#include "Shader.h"

const int VERTEX_COUNT = 24, INDEX_COUNT = 24;
GLfloat vertecies[VERTEX_COUNT] = {
	-0.5f, -0.5f, 0.0f, // Front bottom left 0 
	-0.5f, 0.5f, 0.0f, // Front top left 1 
	0.5f, -0.5f, 0.0f, // Front bottom right 2
	0.5f, 0.5f, 0.0f, // Front top right 3

	-0.5f, -0.5f, -0.5f// Back bottom left 4 
	-0.5f, 0.5f, -0.5f, // Back top left 5
	 0.5f, -0.5f, -0.5f, // Back bottom right 6
	0.5f, 0.5f, -0.5f // Back top right 7

};

GLuint indecies[INDEX_COUNT] = {
	// Front
	0, 1, 3,
	0, 2, 3,

	// Left side
	1, 5, 4,
	4, 0, 1,

	// Right side
	3, 7, 6,
	6, 2, 3,

	// Back
	5, 4, 6,
	5, 7, 6
};

int main() {

	const char* vertexPath = "Shaders/vertexcode.vert.txt";
	const char* fragmentPath = "Shaders/fragmentcode.frag.txt";

	GLWindow window = GLWindow(1200, 800, "Triangle");
	Mesh* mesh = new Mesh();
	Shader* shader = new Shader();

	mesh->CreateObject(vertecies, indecies, VERTEX_COUNT, INDEX_COUNT);
	shader->CreateFromFile(vertexPath, fragmentPath);

	GLuint uniformColor = 0, uniformModel = 0, uniformProjection = 0;
	glm::mat4 projection = glm::perspective(45.0f, (GLfloat)window.GetBufferWidth() / (GLfloat)window.GetBufferHeight(), 0.1f, 100.0f);

	while (!window.WindowShouldClose()) {
		glfwPollEvents();

		float specialColor = (sin(glfwGetTime()) / 2) + 0.5f;
		float specialColor2 = (cos(glfwGetTime()) / 2) + 0.5f;

		glClearColor(specialColor2, specialColor, 0.7f / specialColor, specialColor2);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		uniformColor = shader->ReturnUniformColor();
		uniformModel = shader->ReturnUniformModel();
		uniformProjection = shader->ReturnUniformProjection();

		shader->UseProgram();

		glm::mat4 model(1.0);

		model = glm::translate(model, glm::vec3(0.0f, 0.0f, -2.5f));
		model = glm::rotate(model, (45.0f / 180.0f) * 3.1416f, glm::vec3(1.0f, 1.0f, 0.0f));
		// model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));

		glUniform4f(uniformColor, 0.2f / specialColor, specialColor2, specialColor, specialColor2);
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
		
		mesh->RenderObject();

		shader->UnuseProgram();

		window.SwapBuffers();
	}

	delete shader;
	delete mesh;

	return 0;

}